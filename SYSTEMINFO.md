# National Vulnerabilty Database (NVD)

Vendor: NVD
Homepage: https://nvd.nist.gov/

Product: NVD Vulnerabilities (CVE)
Product Page: https://nvd.nist.gov/vuln

## Introduction
We classify NVD into the Security/SASE domain as NVD provides a solution for identifying vulnerabilties in software and hardware.

## Why Integrate
The NVD adapter from Itential is used to integrate the Itential Automation Platform (IAP) with the NVD API. With this adapter you have the ability to perform operations on items such as:

- CVEs

## Additional Product Documentation
The [API Documents for NVD](https://nvd.nist.gov/developers/vulnerabilities)