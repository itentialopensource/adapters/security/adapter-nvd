
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:15PM

See merge request itentialopensource/adapters/adapter-nvd!11

---

## 0.4.3 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-nvd!9

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:25PM

See merge request itentialopensource/adapters/adapter-nvd!8

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:38PM

See merge request itentialopensource/adapters/adapter-nvd!7

---

## 0.4.0 [05-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-nvd!6

---

## 0.3.3 [03-27-2024]

* Changes made at 2024.03.27_13:42PM

See merge request itentialopensource/adapters/security/adapter-nvd!5

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_16:15PM

See merge request itentialopensource/adapters/security/adapter-nvd!4

---

## 0.3.1 [02-27-2024]

* Changes made at 2024.02.27_11:48AM

See merge request itentialopensource/adapters/security/adapter-nvd!3

---

## 0.3.0 [01-02-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-nvd!2

---

## 0.2.0 [06-02-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-nvd!1

---

## 0.1.1 [11-05-2021]

- Initial Commit

See commit 61f4622

---
