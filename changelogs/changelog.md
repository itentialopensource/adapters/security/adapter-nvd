
## 0.2.0 [06-02-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-nvd!1

---

## 0.1.1 [11-05-2021]

- Initial Commit

See commit 61f4622

---
